package net.kong.yumo.utils;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import net.hasor.core.Inject;
import net.hasor.dataql.UdfSourceAssembly;


@Component
public class EhcacheConfig  implements UdfSourceAssembly{

    @Inject("myJdbcTemplate")
    JdbcTemplate j1;
    
    public EhcacheConfig() {
    	
    }

	public void setJ1(JdbcTemplate j1) {
		this.j1 = j1;
	}

	public void updateEhcacheUser() {
		 List<Map<String, Object>> uList = j1.queryForList("select  *  from dataway_user");

//		 EhcacheUtil.getInstance().setEhcache("dataway_user", uList);
		 for(Map<String, Object> u : uList) {
			 EhcacheUtil.getInstance().setEhcache("dataway_user_"+u.get("user_id").toString(), u);
		 }
		 
	}
	public void updateEhcacheUserByDay(int day) {
		 List<Map<String, Object>> uList = j1.queryForList("select  *  from dataway_user du where du.create_time > to_char(sysdate-"+day+",'YYYY-MM-DD HH24:MI:SS')");
		 
		 System.out.println("updateEhcacheUserByDay:"+uList.size());	  
		 for(Map<String, Object> u : uList) {
			 EhcacheUtil.getInstance().setEhcache("dataway_user_"+u.get("user_id").toString(), u);

			 System.out.println("dataway_user_"+u.get("user_id").toString());	 
			 System.out.println(u);	 
		 }
		 
	}
	public Map<String, Object> getEhcacheUser(String userId) {		   
		System.out.println("get dataway_user");	  
		Object u =null;
		Map<String, Object> re =null;
		try {
			u =   EhcacheUtil.getInstance().getEhcache("dataway_user_"+userId);
			
			re = (Map<String, Object>) u;
			
			System.out.println(re);	 
			}
		catch( Exception e) {
			System.out.println("dataway_user_"+userId);	 
		} 
		 return re;
	}

	public Map<String, Object> getEhcacheVal(String key) {		   
		return getEhcacheValByCacheNameAndKey(EhcacheUtil.getInstance().cacheName,  key);
	}

	public Map<String, Object> getEhcacheValByCacheNameAndKey(String cacheName,String key) {		   
		System.out.println("get EhcacheValByCacheNameAndKey");	  
		Object u =null;
		Map<String, Object> re =null;
		try {
			u =   EhcacheUtil.getInstance().getEhcache(cacheName, key);
			
			re = (Map<String, Object>) u;
			
			System.out.println(re);	 
			}
		catch( Exception e) {
			System.out.println(key);	 
		} 
		 return re;
	}
}
