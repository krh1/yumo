package net.kong.yumo.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 属性元数据描述
 * 
 * @author 张二伟
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PropertyMeta {
    String name();

    String remark() default "";

    String dataType() default "";

    String coding() default "";
    
    int orderNo() default 0;

    char align() default 'C';

    char sortType() default 'A';

    int width() default 80;

    boolean isInput() default true;

    boolean isUpdate() default true;

    boolean isQuery() default true;

    boolean isList() default true;

    boolean isSort() default true;

    boolean isRetrieve() default true;

    boolean isFlowNo() default false;

    boolean isArchiveNo() default false;

    boolean isSystem() default true;

    boolean isUsed() default false;

    String queryType() default "=";
}
