package net.kong.yumo.utils;

import org.springframework.beans.factory.annotation.Autowired;

import redis.clients.jedis.JedisCluster;


/**
* RedisTemplate 工具类
*
* @author coderBing
* @version 2019/04/28
*/
public class RedisUtil {
	
	@Autowired
	private JedisCluster jedisCluster;

	public JedisCluster getJedisCluster() {
		return jedisCluster;
	}

	public void setJedisCluster(JedisCluster jedisCluster) {
		this.jedisCluster = jedisCluster;
	}

	/*
	 * 将值放入缓存
	 * @param key   键
	 * @param value 值
	 */
	public void set(String key, String value) {
		jedisCluster.set(key, value);
	}
	
	/**
	 * 设置有效时间
	 * @param key
	 * @param value
	 * @param nxxx：nxxx NX|XX, NX -- Only set the key if it does not already exist. XX -- 
	 * 		       Only set the key if it already exist.
	 * 			   (只有key不存在时才把key value set到redis; 只有 key存在时，才把key value set到redis)
	 * @param expx：expx EX|PX, expire time units: EX = seconds; PX = milliseconds
	 * @param time
	 */
	public void set(String key, String value, String nxxx, String expx, long time) {
		jedisCluster.set(key, value, nxxx, expx, time);
	}

	/*
	 * 从缓存中取值
	 * @param  key   键
	 * @return value 值
	 */
	public String get(String key) {
		return jedisCluster.get(key);
	}

	/**
	 * 从缓存中删除值
	 * @param key
	 * @return
	 */
	/*public Long delete(String key) {
		return jedisCluster.del(key);
	}*/
}
