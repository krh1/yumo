package net.kong.yumo.assa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import net.hasor.spring.boot.EnableHasor;
import net.hasor.spring.boot.EnableHasorWeb;
import net.hasor.spring.boot.WorkAt;

/**
 * main 
 *
 */
@EnableHasor(useProperties = true,scanPackages = { "net.kong.yumo.assa.hasor.*" })
@EnableHasorWeb(at = WorkAt.Interceptor)
@SpringBootApplication
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class App 
{
    public static void main(String[] args)
    {
        SpringApplication.run(App.class, args);
    }

}
