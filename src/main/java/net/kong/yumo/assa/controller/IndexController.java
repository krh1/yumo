package net.kong.yumo.assa.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class IndexController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);
	 @GetMapping(value = "/user/{name}")
	 public String login(@PathVariable("name") String name){
	        return name+"登陆成功！";
	 }
	 
    @RequestMapping("/xxx/{name}")
    public String loginout(@PathVariable("name") String name){
        return name+"退出成功！";
    }
}
