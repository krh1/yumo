package net.kong.yumo.assa.interceptor;

import java.nio.charset.Charset;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import redis.clients.jedis.JedisCluster;

//@SuppressWarnings("deprecation")
//@Configuration
public class WebSecurityConfig extends WebMvcConfigurerAdapter {

	@Autowired
	@Qualifier("myJedisCluster")
	private JedisCluster jedisCluster;
//	@Bean
    public DatawayInterceptor getSecurityInterceptor() {
        return new DatawayInterceptor(jedisCluster);
    }

//    1、addInterceptors：拦截器
//    addInterceptor：需要一个实现HandlerInterceptor接口的拦截器实例
//    addPathPatterns：用于设置拦截器的过滤路径规则
//    excludePathPatterns：用于设置不需要拦截的过滤规则
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration interceptor = registry.addInterceptor(getSecurityInterceptor());

        interceptor.addPathPatterns("/yumo/**"); //接口路径
        interceptor.addPathPatterns("/swagger-ui.html*"); //swagger路径
        interceptor.excludePathPatterns("/unauthorized"); //未授权页面
        
    }

    
    //spring boot层 中文乱码解决方案1
//    @Bean
//    public HttpMessageConverter<String> responseBodyConverter() {
//        StringHttpMessageConverter converter = new StringHttpMessageConverter(
//                Charset.forName("UTF-8"));
//        return converter;
//    }
//
//    @Override
//    public void configureMessageConverters(
//            List<HttpMessageConverter<?>> converters) {
//        super.configureMessageConverters(converters);
//        converters.add(responseBodyConverter());
//    }
//
//    @Override
//    public void configureContentNegotiation(
//            ContentNegotiationConfigurer configurer) {
//        configurer.favorPathExtension(false);
//    }
}