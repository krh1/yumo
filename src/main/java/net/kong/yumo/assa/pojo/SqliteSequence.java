package net.kong.yumo.assa.pojo;

public class SqliteSequence {
		private String name;
		private Integer seq;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getSeq() {
			return seq;
		}
		public void setSeq(Integer seq) {
			this.seq = seq;
		}
}
