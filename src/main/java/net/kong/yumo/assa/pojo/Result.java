package net.kong.yumo.assa.pojo;

import java.lang.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 

public class Result {
	 private static final Logger log = LoggerFactory.getLogger(Result.class);
	 
	private Integer code=0;
	private String msg="";
	private Object data=null;
	
	@Override
	public String toString() {
		return "Result [code=" + code + ", msg=" + msg + ", data=" + data + ", getCode()=" + getCode() + ", getMsg()="
				+ getMsg() + ", getData()=" + getData() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
}
