package net.kong.yumo.assa.hasor.config;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.AprLifecycleListener;
import org.apache.coyote.http11.Http11AprProtocol;
import org.apache.coyote.http11.Http11NioProtocol;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;

@Configuration
public class ConfigBean {

	private static Logger log = LoggerFactory.getLogger(ConfigBean.class);

	public ConfigBean() {
		
	}
	 @Bean
	    public TomcatServletWebServerFactory servletContainer() {
	        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory()
//	        {
//	            @Override
//	            protected void postProcessContext(Context context) {
//	                SecurityCollection collection = new SecurityCollection();
//					//http方法
//					collection.addMethod("PUT");
//					collection.addMethod("DELETE");
//					collection.addMethod("HEAD");
//					collection.addMethod("OPTIONS");
//					collection.addMethod("TRACE");
//					//url匹配表达式
//					collection.addPattern("/*");
//	                SecurityConstraint constraint = new SecurityConstraint();
//	                constraint.setUserConstraint("CONFIDENTIAL");
//					constraint.addCollection(collection);
//					constraint.setAuthConstraint(true);
//					context.addConstraint(constraint );
//					
//					//设置使用httpOnly
//					context.setUseHttpOnly(true); 
//	            }
//	        }
	        ; 
	        tomcat.setProtocol("org.apache.coyote.http11.Http11AprProtocol");
	        tomcat.addContextLifecycleListeners(new AprLifecycleListener());
	         
			 // 使用对应工厂类提供给我们的接口定制化我们的tomcat connector
	        tomcat.addConnectorCustomizers(new TomcatConnectorCustomizer() {
	            @Override
	            public void customize(Connector connector) {
	            	//apr默认只监听IPv6 [::]
	            	connector.setAttribute("address", "0.0.0.0");
	            	Http11AprProtocol protocol = (Http11AprProtocol) connector.getProtocolHandler();
//	            	Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
	            	
	                // 定制化keepAliveTimeout，设置30秒内没有请求则服务端自动断开keepalive链接
	                protocol.setKeepAliveTimeout(300000);
	                // 当客户端发送超过10000个请求则自动断开keepalive链接
	                protocol.setMaxKeepAliveRequests(10000);
	                //设置最大连接数
	                protocol.setMaxConnections(10000);
	                //设置最大线程数
	                protocol.setMaxThreads(400);
	                //连接超时时间
	                protocol.setConnectionTimeout(30000);
	                //等待队列长度，当可分配的线程数全部用完之后，后续的请求将进入等待队列等待，等待队列满后则拒绝处理，默认100。
	                protocol.setAcceptCount(500);
	                //最小工作线程数，初始化分配线程数，默认10
	                protocol.setMinSpareThreads(100);
	                //默认配置下，触发的请求超过800+500后拒绝处理(最大工作线程数+等待队列长度) 
	            }
	        });
	        return tomcat;
	    }
//	@Primary
//	@Bean("myJedisCluster")
	 public JedisCluster myJedisCluster(//
	           @Value("${spring.redis.cluster.nodes}") String clusterNodes,       //
	            @Value("${spring.redis.password}") String password   //
	          ) throws SQLException {
	    int  connectionTimeout = 50000;
	    int  soTimeout = 50000;
	    int  maxAttempts = 5;
		String[] cNodes = clusterNodes.split(",");
		Set<HostAndPort> nodes = new HashSet<>();
			//分割出集群节点
		for (String node : cNodes) {
				String[] hp = node.split(":");
				nodes.add(new HostAndPort(hp[0], Integer.parseInt(hp[1])));
		}
		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
		jedisPoolConfig.setMaxIdle(20);
		jedisPoolConfig.setMinIdle(1);
		jedisPoolConfig.setMaxWaitMillis(-1);
		jedisPoolConfig.setMaxTotal(1000);
		jedisPoolConfig.setTestOnBorrow(true);
		
		JedisCluster redisCluster = new JedisCluster(nodes, connectionTimeout, soTimeout, maxAttempts, password, jedisPoolConfig);
		 log.info("redisCluster set----------->"+redisCluster.set("活性", "1"));
			
		return redisCluster;
	   }

	
	
}
