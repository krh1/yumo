package net.kong.yumo.assa.hasor.chainspi;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.hasor.dataway.spi.ApiInfo;
import net.hasor.dataway.spi.ResultProcessChainSpi;
import net.hasor.dataway.spi.PreExecuteChainSpi;
import net.hasor.dataway.spi.StatusMessageException;
import net.hasor.utils.future.BasicFuture;
import net.hasor.web.MimeType;

public class MyPreExecuteChainSpi implements  PreExecuteChainSpi ,ResultProcessChainSpi{//
 
	private static Logger logger = LoggerFactory.getLogger(MyPreExecuteChainSpi.class);

	public MyPreExecuteChainSpi() {
		
	}

	@Override
	public Object callAfter(boolean formPre, ApiInfo apiInfo, Object result) {
        // formPre 为 true，表示 preExecute 已经处理过。
        // apiInfo.isPerform() 为 true 表示，API 调用是从 UI 界面发起的。

		String path = apiInfo.getApiPath();
		if(!"/api/heartbeat/v1".equals(path)) {	
        logger.info("----callAfter----Print Current result: "+result.toString());
 		}
        return result;
    }
	
	
	 public Object callError(boolean formPre, ApiInfo apiInfo, Throwable e) {
		Map re= new HashMap<String, Object>() {{
	            put("code", "-1");
	            put("method", apiInfo.getMethod());
	            put("path", apiInfo.getApiPath());
	            put("msg", e.getMessage());
	            put("Throwablemsg", e);
	        }} ;
		 logger.info("----callError----: "+re);
	     return re;
	  }
	 
	 
	@SuppressWarnings("deprecation")
	@Override
	public void preExecute(ApiInfo apiInfo, BasicFuture<Object> future) {
		// TODO Auto-generated method stub
        // apiInfo.isPerform() 为 true 表示，API 调用是从 UI 界面发起的。
		String path = apiInfo.getApiPath();
		
		if(!"/api/heartbeat/v1".equals(path)) {			
			boolean  isPerform=apiInfo.isPerform();		
			Map<String, Object> maps = apiInfo.getParameterMap();

			logger.info("---preExecute-----Print Current  Uri and method: " +path ); 
//			logger.info("---preExecute-----Print Current isPerform: " +isPerform ); 
//			logger.info("---preExecute-----Print Current getParameterMap: " +maps.keySet() ); 
			
			for (String key:maps.keySet()) {
				logger.info("---preExecute-----Print Current ParameterMap: " +key + "：" + maps.get(key));
			}	
		}
		
		
	}



}
