package net.kong.yumo.assa.hasor.config;

import java.sql.SQLException;

import javax.sql.DataSource;
import javax.transaction.UserTransaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.jta.JtaTransactionManager;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.icatch.jta.UserTransactionManager;

import net.hasor.core.Inject;

public class transactionTemplateHasor {

	private static Logger log = LoggerFactory.getLogger(transactionTemplateHasor.class);
	

	@Inject("jtaTransactionTemplate")
	TransactionTemplate jtatt=null;

	@Inject("ds1SysJdbcTemplate")
	NamedParameterJdbcTemplate jta1Sys=null;
	@Inject("ds2daglJdbcTemplate")
	NamedParameterJdbcTemplate jta2dagl=null;

	@Inject()
	JtaTransactionManager xatx=null;
	
	public transactionTemplateHasor() {
		
	}

    public void init() throws SQLException {
    	log.info("----------------init-----transactionTemplateHasor--------------");
    }
    
	
	
	public TransactionTemplate getJtatt() {
		return jtatt;
	}

	public void setJtatt(TransactionTemplate jtatt) {
		this.jtatt = jtatt;
	}

	 

	public NamedParameterJdbcTemplate getJta1Sys() {
		return jta1Sys;
	}

	public void setJta1Sys(NamedParameterJdbcTemplate jta1Sys) {
		this.jta1Sys = jta1Sys;
	}

	public NamedParameterJdbcTemplate getJta2dagl() {
		return jta2dagl;
	}

	public void setJta2dagl(NamedParameterJdbcTemplate jta2dagl) {
		this.jta2dagl = jta2dagl;
	}

	public JtaTransactionManager getXatx() {
		return xatx;
	}

	public void setXatx(JtaTransactionManager xatx) {
		this.xatx = xatx;
	}

 	
}
