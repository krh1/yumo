package net.kong.yumo.assa.hasor.config;

import java.util.Set;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import net.hasor.core.ApiBinder;
import net.hasor.core.DimModule;
import net.hasor.core.Module;
//import net.hasor.dataway.authorization.AuthorizationType;
import net.hasor.dataway.spi.AuthorizationChainSpi;
import net.hasor.dataway.spi.PreExecuteChainSpi;
import net.hasor.dataway.spi.ResultProcessChainSpi;
import net.hasor.dataway.spi.SerializationChainSpi;
import net.kong.yumo.assa.hasor.chainspi.MyPreExecuteChainSpi;

/**
 * //注册接口拦截器
 * @author Administrator
 *
 */
@DimModule
@Component
public class ExampleModule implements Module {
 public void loadModule(ApiBinder apiBinder) throws Throwable {
	// 配置所有接口，都是只读权限
//	 final Set<String> codeSet = AuthorizationType.Group_ReadOnly.toCodeSet();
	 MyPreExecuteChainSpi MyPreExecuteChainSpi=new MyPreExecuteChainSpi(); 
	 apiBinder.bindSpiListener(ResultProcessChainSpi.class, MyPreExecuteChainSpi);
     apiBinder.bindSpiListener(PreExecuteChainSpi.class, MyPreExecuteChainSpi);
 }


}
