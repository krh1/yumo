package net.kong.yumo.assa.hasor.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.jta.JtaTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import net.hasor.core.ApiBinder;
import net.hasor.core.DimModule;
import net.hasor.db.JdbcModule;
import net.hasor.db.Level;
import net.hasor.spring.SpringModule;

@DimModule
@Component
public class StartModel  implements SpringModule {
	
	@Autowired()
	@Qualifier("ds4DataSource")
	private DataSource dataSource;

	@Autowired
	@Qualifier("ds1DataSource")
	private DataSource ds1SysDataSource;
	@Autowired
	@Qualifier("ds3DataSource")
	private DataSource ds2daglDataSource;
	@Autowired
	@Qualifier("myJtaTransactionManager")
	private JtaTransactionManager jtaTransactionManager;

	@Override
	public void loadModule(ApiBinder apiBinder) throws Throwable {
		// TODO Auto-generated method stub
		apiBinder.installModule(
				new JdbcModule(
						Level.Full, this.dataSource)); //默认数据源
		//自定义事务使用相关数据源，TransactionManager，JdbcTemplate,TransactionTemplate
//		apiBinder.bindType(DataSource.class).bothWith("ds1SysDataSource").nameWith("ds1SysDataSource")
//		.toInstance( this.ds1SysDataSource);
//		apiBinder.bindType(DataSource.class).bothWith("ds2daglDataSource").nameWith("ds2daglDataSource")
//		.toInstance( this.ds2daglDataSource);
		apiBinder.bindType(JtaTransactionManager.class).bothWith("myJtaTransactionManager").nameWith("myJtaTransactionManager")
		.toInstance( this.jtaTransactionManager);
		
		TransactionTemplate transactionTemplate=new TransactionTemplate(this.jtaTransactionManager);
		apiBinder.bindType(TransactionTemplate.class).idWith("jtaTransactionTemplate").nameWith("jtaTransactionTemplate")
		.toInstance(transactionTemplate);
		
		apiBinder.bindType(NamedParameterJdbcTemplate.class).idWith("ds1SysJdbcTemplate").nameWith("ds1SysJdbcTemplate")
		.toInstance(new NamedParameterJdbcTemplate(this.ds1SysDataSource));
		apiBinder.bindType(NamedParameterJdbcTemplate.class).idWith("ds2daglJdbcTemplate").nameWith("ds2daglJdbcTemplate")
		.toInstance(new NamedParameterJdbcTemplate(this.ds2daglDataSource));
		
		apiBinder.bindType(transactionTemplateHasor.class)
        .initMethod("init")    // 初始化方法，相当于 @Init 注解
        .asEagerSingleton();   // 单例，相当于 @Singleton 注解
  

	}

}
