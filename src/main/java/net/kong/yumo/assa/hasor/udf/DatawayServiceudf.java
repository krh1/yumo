package net.kong.yumo.assa.hasor.udf;

import net.hasor.core.AppContext;
import net.hasor.core.Inject;
import net.hasor.dataql.UdfSourceAssembly;
import net.hasor.dataway.DatawayService;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatawayServiceudf implements UdfSourceAssembly {

	private static Logger logger = LoggerFactory.getLogger(DatawayServiceudf.class);
    @Inject
    DatawayService dataway;
    
    public Map<String, Object> getPost(String url,Map<String ,Object> map){
    	logger.info("url======"+url);
    	logger.info("map======"+map);

        Map<String, Object> result = new HashMap<>();
        // 结果
        try {
            result.putAll((Map<String, Object>) dataway.invokeApi("post", url, map)) ;
        } catch (Throwable throwable) {
            throwable.printStackTrace(); 
            result.put("Emsg", throwable.getMessage());result.put("code", "-1");
        }
        return result;
    }


    public Map<String, Object> getGet(String url,Map<String ,Object> map){
    	logger.info("url======"+url);
    	logger.info("map======"+map);
        Map<String, Object> result = new HashMap<>();
        // 结果
        try { result.putAll((Map<String, Object>) dataway.invokeApi("get", url, map)) ;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            result.put("Emsg", throwable.getMessage()); result.put("code", "-1");
        }
        return result;
    }
    public Object  getPostVal(String url,Map<String ,Object> map){
    	logger.info("url======"+url);
    	logger.info("map======"+map);
        Map<String, Object> result = new HashMap<>();
        // 结果
        try {
            result.putAll((Map<String, Object>) dataway.invokeApi("post", url, map)) ;
        } catch (Throwable throwable) {
            throwable.printStackTrace(); 
            result.put("Emsg", throwable.getMessage());result.put("code", "-1");
        }
        return result.get("value");
    }


    public Object getGetVal(String url,Map<String ,Object> map){
    	logger.info("url======"+url);
    	logger.info("map======"+map);
        Map<String, Object> result = new HashMap<>();
        // 结果
        try { result.putAll((Map<String, Object>) dataway.invokeApi("get", url, map)) ;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            result.put("Emsg", throwable.getMessage()); result.put("code", "-1");
        }
        return result.get("value");
    }





}
