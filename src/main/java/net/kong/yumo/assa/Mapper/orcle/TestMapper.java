package net.kong.yumo.assa.Mapper.orcle;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import net.kong.yumo.assa.Mapper.baseMapper; 

@Repository 
public interface TestMapper  extends  baseMapper {

    @Insert("insert into `T_KHDA_EMAIL_RECORD` tker (tker.id,tker.receive_add,tker.email_title,tker.email_content)"+
            "values(#{id},#{receiveAdd},#{emailTitle},#{emailContent})")
    public int addEmail(@Param("id") String id, @Param("receiveAdd") Integer receiveAdd,
    		@Param("emailTitle") String emailTitle,@Param("emailContent") String emailContent);
}
