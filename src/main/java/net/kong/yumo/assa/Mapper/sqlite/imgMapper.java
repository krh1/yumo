package net.kong.yumo.assa.Mapper.sqlite;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import net.kong.yumo.assa.Mapper.baseMapper;
import net.kong.yumo.assa.pojo.SqliteSequence;

@Repository 
public interface imgMapper extends  baseMapper {

    @Insert("insert into sqliteSequence  (name,seq) values(#{name},#{seq});")
    public int addEmail( @Param("name") String  name,@Param("seq") Integer seq);
    
    @Select("select * from sqliteSequence limit #{lineNum};")
    public List<SqliteSequence> selLimit(
    		@Param("lineNum") Integer  lineNum);
    
    @Select("select * from sqliteSequence limit #{lineNum}  offSet #{offSet}; ")
    public List<SqliteSequence> selLimit2(
    		@Param("lineNum") Integer  lineNum, @Param("offSet") Integer offSet );
    
    @Select("select * from sqliteSequence ;")
    public List<Map<String,Object>> selTableSqliteSequence( );
   
}


