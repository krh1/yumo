package net.kong.yumo.assa.Mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import net.kong.yumo.assa.pojo.SqliteSequence;
/**
 * 常用基本sql，要注意预防sql注入（$拼接符号，#预编译符号最后会加上''）
 * @author Administrator
 *
 */
@Repository
public interface baseMapper {
	   @Insert("insert into ${tableName}  (${title}) values(${value});")
	    public int addTable( @Param("tableName") String  tableName, @Param("title") String  title,@Param("value") String value);
	    
	    @Select("select * from ${tableName} limit #{lineNum};")
	    public List<Map<String,Object>> selTableLimit( @Param("tableName") String  tableName,
	    		@Param("lineNum") Integer  lineNum);
	    
	    @Select("select * from ${tableName} limit #{lineNum}  offSet #{offSet}; ")
	    public List<Map<String,Object>> selTableLimit2( @Param("tableName") String  tableName,
	    		@Param("lineNum") Integer  lineNum, @Param("offSet") Integer offSet );
	
	    @Select("select * from ${tableName};")
	    public List<Map<String,Object>> selTableMapPram( @Param("tableName") String  tableName);
	   
	    @Select("select * from ${tableName} where id=#{id} ")
	    public List<Map<String,Object>> selTableMapPramStrId( @Param("tableName") String  tableName, @Param("id") String  strId);
	    
	    @Select("select * from ${tableName} where id=#{id} ")
	    public List<Map<String,Object>> selTableMapPramNumId( @Param("tableName") String  tableName, @Param("id") Integer  numId);
	    
	    @Delete("delete  from ${tableName} where id=#{id};")
	    public List<Map<String,Object>> delTableMapPramStrId( @Param("tableName") String  tableName, @Param("id") String  strId);

	    @Delete("delete  from ${tableName} where id=#{id};")
	    public List<Map<String,Object>> delTableMapPramNumId( @Param("tableName") String  tableName, @Param("id") Integer  numId);
	    
	    @Update("update ${tableName} set ${columnName}=#{val}  where id=#{id};")
	    public List<Map<String,Object>> uptateTableMapPramId( @Param("tableName") String  tableName, @Param("columnName") String  columnName,
	    		@Param("val") String  val, @Param("id") String  id);
	    
}
