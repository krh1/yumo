package net.kong.yumo.assa.Mapper.mysql;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
 

@Configuration
@MapperScan(value = "net.kong.yumo.assa.Mapper.mysql",sqlSessionTemplateRef = "ds1SqlSessionTemplate")
public class MysqlDataSourceConfig {
	  private static Logger log = LoggerFactory.getLogger(MysqlDataSourceConfig.class);
	  
	  /**
	     * 创建sqlSessionFactory
	     * @param dataSource
	     * @return
	     * @throws Exception
	     */
	    @Bean(name = "ds1SqlSessionFactory")
	    public SqlSessionFactory memberSqlSessionFactory(@Qualifier("ds1DataSource") DataSource dataSource) throws Exception {
	        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
	        sqlSessionFactoryBean.setDataSource(dataSource);
	        return sqlSessionFactoryBean.getObject();
	    }

	  
	    
	 
	    /**
	     * 创建sqlSession模板
	     * @param sqlSessionFactory
	     * @return
	     */
	    @Bean(name = "ds1SqlSessionTemplate")
	    public SqlSessionTemplate memberSqlSessionTemplate(@Qualifier("ds1SqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
	        return new SqlSessionTemplate(sqlSessionFactory);
	    }
	    
}
