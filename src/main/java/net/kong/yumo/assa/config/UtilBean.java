package net.kong.yumo.assa.config;

import java.text.SimpleDateFormat;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UtilBean {
	 @Bean(name = "UtilSimpleDateFormatyyyyMMdd")
	    public SimpleDateFormat testSqlSessionFactory(@Qualifier("ds2DataSource") DataSource dataSource)  {
 	        return new SimpleDateFormat("yyyyMMdd");
	    }

}
